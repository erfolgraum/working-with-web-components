import { creator, email, form, btn } from "./join-us-section.js";
import { storeToLocalStorage } from "./store-to-localstorage.js"
import "./styles/style.css";
import { getUsers } from "./ajax.js";
import "./web-component.js";



/* eslint no-unused-vars: "off" */
/* eslint quotes: ["error", "single"] */
/* eslint prefer-arrow-callback: "off" */
/* eslint func-names: "off" */
document.addEventListener("DOMContentLoaded", function () {
  // creator.create('advanced')
  creator.create("standard");
  // creator.create().remove()

  form.addEventListener("input", (e) => {
    e.preventDefault();
    // const nameOnly = savedEmail.substring(0, savedEmail.indexOf("@"));
    storeToLocalStorage(e.target.value);
  });
 
    if(localStorage.getItem("email") === null) {
      email.value = "";
      email.style.display = "inline-block"; 
    }
    if(email.value !== null){
      email.value = localStorage.getItem("email")
    }
    // if(localStorage.getItem("email") !== null && email.value !== null) {
    //     email.value = localStorage.getItem("email");
    // }

    // if(localStorage.getItem("email") !== null && email.value !== null) {
    //     email.value = localStorage.getItem("email");
    // }

    if(localStorage.getItem('email') && localStorage.getItem('subscribe')) {
      email.style.display = "none"
      btn.setAttribute("value", "Unsubscribe"); 
    }

    getUsers('/api/community')    
});


